CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * FAQ
 * Maintainers

INTRODUCTION
------------

This Cloudflare Stream hosted video media type creates the hosted_video media
type & was integrated in the cloudflare_stream module.

The sole purpose of this module is for me to get the vetted role on drupal.org.
Please do not use this module in your project but get & enable it via the
cloudflare_stream module.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cloudflare_stream_hosted_video

 * To submit bug reports and feature suggestions, or track changes please go to
   the main module issue queue:
   https://www.drupal.org/node/add/project-issue/cloudflare_stream

REQUIREMENTS
------------

This module requires the following modules:

 * [Cloudflare Stream](https://www.drupal.org/project/cloudflare_stream)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. When enabled, the module will
automatically install the needed configuration.

FAQ
---

Q: Is a Cloudflare account needed to test this?

A: No, an account isn't needed to test & install this module, but bear in mind
   this media type will only show videos of the Cloudflare Stream type. Without
   the needed settings added to the Cloudflare Stream config page, no videos
   will be shown.


MAINTAINERS
-----------

Current maintainers:
 * Pieter-Jan Baert (pjbaert) - https://www.drupal.org/user/2869357

This project has been sponsored by:
 * Calibrate
   In the past fifteen years, Calibrate have gained large expertise in
   consulting organizations in various industries on developing tailor made
   solutions using various open source tools and technologies.
   Visit https://www.calibrate.be for more information.
