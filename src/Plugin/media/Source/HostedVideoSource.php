<?php

namespace Drupal\cloudflare_stream_hosted_video\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;

/**
 * A media source plugin for hosted video assets.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "hosted_video",
 *   label = @Translation("Hosted Video"),
 *   description = @Translation("A media source plugin for hosted video assets."),
 *   allowed_field_types = {"cloudflarevideo"},
 *   default_thumbnail_filename = "video.png",
 * )
 */
class HostedVideoSource extends File {

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $file_extensions_service = \Drupal::service('cloudflare_stream.file_extensions');
    return parent::createSourceField($type)->set('settings', ['file_extensions' => $file_extensions_service->listAllowedFileExtensions()]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'cloudflarevideo_default',
      'label' => 'visually_hidden',
    ]);
  }

}
